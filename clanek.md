---
title: "Integrace EET do Nette 3"
description: "Elektronická evidence tržeb může být strášákem většiny z nás, kdo chtějí příjmat hotovost. Usnadnil jsem celou integraci do Nette 3 nejen vývojem samotné knihovny, ale i vývojem balíčku pro zprovoznění pomocí Nette."
keywords: "eet, elektronická evidence tržeb"
authors:
	- 
		name: "Filip Šedivý"
		email: mail@filipsedivy.cz
		website: https://filipsedivy.cz
		github: https://github.com/filipsedivy
		twitter: https://twitter.com/filipsedivy
		gravatar: https://www.gravatar.com/avatar/895cd5ba184b5b611d883ea7f034432e
---

## Instalace závislosti

Pokud řešite problém jak elegantně integrovat EET do Vašeho projektu, tak si již nemusíte lámat hlavu. Stačí využít balíček [contributte/eet](https://github.com/contributte/eet) který přímo integruje elektronickou evidenci tržeb do vašeho projektu.

Jakmile si pomocí composeru nainstalujeme závislost, je potřeba ji registrovat ve vašem neon souboru. To uděláme pomocí jednoduchého kódu ve kterém definujeme rozšíření.

```yaml
extensions:
	eet: Contributte\EET\DI\EETExtension
```

Aktuálně je v naší Nette aplikaci registrována knihovna pro elektronickou evidenci tržeb. Nicméně jenom tento krok nestačí. 

## Konfigurace knihovny

Dále je potřeba nastavit certifikát, který následně přiřazuje tržby ke konkrétnímu podnikateli. Pokud tento certifikát nemáte, je možné jej zažádat elektronicky pomocí daňového portálu [ADIS](http://adisspr.mfcr.cz/). Proto aby knihovna s certifikátem mohla správně pracovat, je nutné aby certifikát měl koncovku .p12. Certifikát s touto koncovkou lze stáhnout skrze daňový portál, a není potřeba jej jakkoliv upravovat nebo převádět. S jiným formátem nedokáže knihovna pracovat.

Pokud máte certifikát stažený a uložený v projektu, nyní je potřeba nastavit cestu a heslo. To uděláte pomocí konfiguračních parametrů opět v neon souboru.

```yaml
eet:
	certificate: 
		path: %appDir%/../private/eet.p12
		password: my-password
```

## Použití v aplikaci

Nyní se přesueneme na místo, kam chceme vložit závislost knihovny. Já využiji ukázkový presenter *SomePresenter*.

```php
use Contributte\EET;
use FilipSedivy;
use Nette;

final class SomePresenter extends Nette\Application\UI\Presenter
{
    /** @var EET\Dispatcher */
    private $client;

    public function injectClientFactory(EET\ClientFactory $factory)
    {
        $this->client = $factory->create();
    }

    public function processPayment()
    {
        $receipt = new FilipSedivy\EET\Receipt();

        $this->client->send($receipt);

      	$fik = $this->client->getFik();
        $pkp = $this->client->getPkp();
    }
}
```



V presenteru *SomePresenter* jsme si nyní předali závislost pro třídu *Contributte\EET\ClientFactory* který vytváří instanci *Contributte\EET\Dispatcher*. V případě, že při vytváření továrny dojde chyba, nespadne celá aplikace, ale jsme schopni tyto chyby odchytnout při vytváření právě této továrny.

Mezi chyby mohu zařadit třeba chybné heslo k certifikátu nebo neplatný certifikát. 

Při provádění platby, je potřeba i myslet na možné výjimky které mohou vzniknout při registraci účtenky. Ošetření chyb je možno provést podle [oficiální dokumentace](https://github.com/contributte/eet/blob/master/.docs/README.md#client-usage).

## Vychytávky šetřící čas

Pokud máte jen malý obchod, a nechcete neustále vyplňovat DIČ nebo jiné parametry, tak v konfiguračním souboru je pro toto vyhrazená část *receipt*.

Například vlastním malý obchod a jako číslo pokladního zařízení mám 1989 a moje DIČ je CZ01234. Proto abych tyto hodnoty nemusel furt zadávat, tak si je mohu nechat předvyplnit.

```yaml
eet:
	receipt:
	id_pokl: 1989
	dic_popl: CZ01234
```

Poté je potřeba v kódu upravit volání třídy Receipt do které se vyplňují údaje, které se následně předávají na  daňovou správu. Instanci třídy nebudeme vytvářet pomocí klíčového slova *new*, ale budeme ji volat pomocí továrny která nám právě tyto údaje předvyplní. 

```php
use Contributte\EET;
use Nette;

final class SomePresenter extends Nette\Application\UI\Presenter
{
    /** @var EET\ReceiptFactory */
    private $receiptFactory;

    public function injectReceiptFactory(EET\ReceiptFactory $factory)
    {
        $this->receiptFactory = $factory;
    }

    public function processPayment()
    {
        $receipt = $this->receiptFactory->create();
    }
}
```

Takto vytvořená instance má vlastnosti *Contributte\EET\Receipt* tak že jde s údaji nadále klasicky pracovat. 

Rozdíl mezi instancí *Contributte\EET\Receipt* a *FilipSedivy\EET\Receipt* je ten, že v instanci knihovny Receipt je také nastavené **uuid_zpravy** a **aktuální datum a čas tržby**. Tedy tyto hodnoty není třeba znovu nastavovat. Stačí si je jen zpětně vytáhnout a uložit si do databáze.

## Testovací verze

Pokud potřebujete celý proces platby otestovat, je možné využit *playground* rozhraní, které využívá certifikáty daňové správy. Takto provedené platby nejsou registrovány pod vaším podnikatelským účtem. Nicméně i tak vrací fiskální kód.

Nakonec si ukážeme možnost, jak může vypadat konfigurační kód pro oddělení produkční části od testovací.

```yaml
# config.neon

extensions:
	eet: Contributte\EET\DI\EETExtension
    
eet:
	certificate:
	path: %appDir%/../eet.p12
	password: my-password

	dispatcher:
		service: production
		
	receipt:
		dic_popl: CZ01234

# config.local.neon

eet:
	certificate:
		path: %appDir%/../etrzby-eet.p12
		password: eet
    
	dispatcher:
		service: playground

	receipt:
		dic_popl: CZ00000019
		id_pokl: IP105
		id_provoz: id_provoz
```

